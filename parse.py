# -*- coding: utf-8 -*-
import requests
import sys
from lxml import html


class ScheduleParser(object):
    PURPLE = '\033[95m'
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    ORANGE = '\033[93m'
    RED = '\033[91m'
    WHITE = '\033[0m'

    URLS = {
        'navi-gaming': 'http://watch.navi-gaming.com',
        'starladder': 'http://live.starladder.tv/ru/matches/series/star'
    }
    GAMES = {
        'd2': 'Dota',
        'cs': 'Counter-Strike'
    }

    TEAMS = {
        'navi': {
            'name': 'Na\'Vi',
            'color': ORANGE
        },
        'vpp': {
            'name': 'VirtusPro Polar',
            'color': ORANGE
        }

    }
    versus = ' vs'

    def normalize_keys(self):
        keys = sys.argv
        del(keys[0])
        new_keys = list()
        if len(keys) > 0:
            for k in keys:
                new_keys.append(k.replace('-', ''))
        return new_keys

    def get_navi_schedule(self):
        response = requests.get(self.URLS['navi-gaming'])
        parsed_body = html.fromstring(response.text)
        dates = parsed_body.find_class('date')
        matches = []
        row = None
        keys = self.normalize_keys()
        for date in dates:
            for c in date.getnext().getchildren():
                row = c
                game = row.getchildren()[0].attrib['title']
                opponent = self.RED + c.find_class('team')[1].text_content()
                time_in = u' через %s' % c.find_class('inline_block')[0].text.strip()
                text = self.GREEN + c.attrib['title'] + self.WHITE
                matches.append({'game': game,
                                'opponent': opponent,
                                'time_in': time_in,
                                'text': text})
        if len(keys) > 0:
            schedule = []
            for m in matches:
                for k in keys:
                    if self.GAMES[k] in m['game']:
                        schedule.append(m)
            return schedule
        else:
            return matches

    def get_starladder_schedule(self):
        pass

    def print_navi_schedule(self):
        navi_schedule = self.get_navi_schedule()
        navi = self.TEAMS['navi']
        navi_title = navi['color'] + navi['name'] + self.versus
        for i in navi_schedule:
            print i['game']
            print navi_title + i['opponent'] + i['time_in']
            print i['text']


p = ScheduleParser()
p.print_navi_schedule()
