# NAVI scheduler
Python (2.7) parser of http://watch.navi-gaming.com/ to get colored shedule of next games of our favorite squads.

Shows games of NAVI squads
> Dota 2
> CS:GO

Dependencies:
> lxml 3.4.1
> requests==2.5.0 

Installation ( via pip ):
> pip install -r requrements.txt

Usage:
> python ~/<your_path_to_repo>/dota-parser/parse.py

Keys:
> -d2 - shows only dota2 squad matches \n
> -cs - shows only cs:go squad matches

Plans:
> add integration with Google Calendar to put matches as events and recieve notifications.
> add other CIS teams ( Team Empire, VP, HR etc.)